#include <iostream>
#include <vector>
#include <allegro5\allegro_primitives.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>
#include <allegro5\allegro_native_dialog.h>
#include <allegro5\allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
using namespace std;


class soundManager{

private:

boolean sound_array[9];
string fighter;
Player *player;
ALLEGRO_SAMPLE *sample;
ALLEGRO_SAMPLE_ID sample_id;

public:

  soundManager(){
    sound_array[0]=true;
  //  sound_array[2]=false;


  }

  boolean canPlaySound(int sound){
    return sound_array[sound];
  }

  void setPlaySound(int sound,boolean arg){
    sound_array[sound]=arg;
  }

  void playSound(int sound){
	  cout<<"PLAYING "<<sound<<endl;
    switch(sound){
      case 0:
        if(sound_array[sound]){
          sample=al_load_sample("menu_sound.wav");
          al_play_sample(sample, 1.0, 0, 1, ALLEGRO_PLAYMODE_LOOP,&sample_id);
        }
        break;
      case 1:
        if(sound_array[sound]){
          sample=al_load_sample("fight.wav");
          al_play_sample(sample, 1.0, 0, 1, ALLEGRO_PLAYMODE_ONCE,NULL);
        }
      break;
      case 2:
      //cout<<"!!!!<<"<<endl;
      if(sound_array[sound]){
        sample=al_load_sample("hit.wav");
        al_play_sample(sample, 1.0, 0, 1, ALLEGRO_PLAYMODE_ONCE,NULL);
        //sound_array[sound]=false;
      }
		break;
		 case 3:
    // if(sound_array[sound]){
       sample=al_load_sample("got_hit.wav");
       al_play_sample(sample, 1.0, 0, 1, ALLEGRO_PLAYMODE_ONCE,NULL);
    // }
		break;
    case 4:
    if(sound_array[sound]){
      sound_array[sound]=false;
      sample=al_load_sample("subzero_win.wav");
      al_play_sample(sample, 1.0, 0, 1, ALLEGRO_PLAYMODE_ONCE,NULL);
    }
   break;
   case 5:
     if(sound_array[sound]){
        sound_array[sound]=false;
       sample=al_load_sample("scorpio_win.wav");
       al_play_sample(sample, 1.0, 0, 1, ALLEGRO_PLAYMODE_ONCE,NULL);
     }
   break;
   case 6:
     sample=al_load_sample("freeze.wav");
     al_play_sample(sample, 1.0, 0, 1, ALLEGRO_PLAYMODE_ONCE,NULL);
     break;

     case 7:
    //
          //sound_array[sound]=false;
          sample=al_load_sample("scorpion_spear.wav");
          al_play_sample(sample, 1.0, 0, 1, ALLEGRO_PLAYMODE_ONCE,NULL);
       //}
       break;

       case 8:
       sample=al_load_sample("fatality.wav");
       al_play_sample(sample, 1.0, 0, 1, ALLEGRO_PLAYMODE_ONCE,NULL);
       break;

       case 9:
       if(sound_array[sound]){
         sound_array[sound]=false;
         sample=al_load_sample("finish.wav");
         al_play_sample(sample, 1.5, 0, 1, ALLEGRO_PLAYMODE_ONCE,NULL);
       }
       break;

    }



  }
  void stopSound(){
	  cout<<"Sound stop"<<endl;
    al_stop_sample(&sample_id);
  }

  void resetSounds(){
    for(int i=0;i<10;i++){
      sound_array[i]=true;
    }
  }


};
