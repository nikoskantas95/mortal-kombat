#ifndef __SPECIAL_MOVE_H_
#define __SPECIAL_MOVE_H_
#include <iostream>
#include <vector>
#include <cmath>
#include <Windows.h>
#include <time.h>
#include <string>
#include "sprite.h"
using namespace std;

class specialMove{
private:
  sprite *sp_sprite;
  vector <int> hitbox;
  int X,Y;
  boolean active;
  //sprite* player_sprite;



public:
  specialMove(sprite *sp){
  //  player=pl;
    active=false;
    sp_sprite=sp;
    X=0;
    Y=0;
  //
	clearHitbox();

  }

  void despawn(){

    sp_sprite->positionX=0;
    sp_sprite->positionY=0;
    X=0;
    Y=0;
    active=false;

    cout<<"Special despawned..."<<endl;

  }

  void updateSpecialPosition(){
    X=sp_sprite->positionX;
    Y=sp_sprite->positionY;
    hitbox.clear();
		hitbox.push_back(sp_sprite->positionX);
		hitbox.push_back(sp_sprite->positionY);
		hitbox.push_back(sp_sprite->width);
		hitbox.push_back(sp_sprite->height);
    //cout<<"Special Position: "<<X<<","<<Y<<endl;
  //  printHitbox();
  }


  void updateHitbox(int x,int y,int width,int height){
		hitbox[0]+=x;
		hitbox[1]+=y;
		hitbox[2]+=width;
		hitbox[3]+=height;
		printHitbox();
	}

  vector <int> getHitbox(){
		return hitbox;
	}

  int getSpecialX(){
  return X;
}

int getSpecialY(){
  return Y;
}

  void printHitbox(){
  cout<<"Hitbox special: "<<"\n<"<<hitbox[0]<<","<<hitbox[1]<<","<<hitbox[2]<<","<<hitbox[3]<<">\n";
}

  void clearHitbox(){
    hitbox.push_back(0);
    hitbox.push_back(0);
    hitbox.push_back(0);
    hitbox.push_back(0);
  }




};
#endif
