#ifndef __ANIMATION_H__
#define __ANIMATION_H__

#include <vector>
#include <map>
#include <fstream>
#include <string>
#include <iostream>
using namespace std;

class frame{
public:
	unsigned x;
	unsigned y;
	unsigned width;
	unsigned height;
	int dx;
	int dy;
	unsigned ticksPerFrame;
	frame(){}

	frame(unsigned X,unsigned Y,unsigned w,unsigned h,int DX, int DY, unsigned tpf){
		x=X;
		width=w;
		y=Y;
		height=h;
		dx=DX;
		dy=DY;
		ticksPerFrame=tpf;
	}

	frame(string str){
		string tmp;
		x=stoi(str.substr(0,str.find("-")));
		tmp=str.substr(str.find("-")+1,str.length());
		y=stoi(tmp.substr(0,tmp.find("-")));
		tmp=tmp.substr(tmp.find("-")+1,tmp.length());
		width =stoi(tmp.substr(0,tmp.find("-")));
		tmp=tmp.substr(tmp.find("-")+1,tmp.length());
		height =stoi(tmp.substr(0,tmp.find("-")));
		//tmp=tmp.substr(tmp.find("/")+1,tmp.length());
		//width =stoi(tmp.substr(0,tmp.find("/")));
		dx=0;
		dy=0;
		ticksPerFrame=1;
		//cout<<"x->"<<x<<" "<<"y->"<<y<<" "<<"width->"<<width<<" "<<"height->"<<height<<endl;


	}




	void toString(){
		cout<<"X:"<<x<<" Y:"<<y<<" W:"<<width<<" H:"<<height<<"clockTicksForAnimation:"<<ticksPerFrame<<endl;
	}
};


class AnimationFilm{
private:
	std::vector<frame> bounds;
	unsigned currFrame;
	unsigned frameSize;


public:
	AnimationFilm(){

	}

	AnimationFilm(string line){


		string x,y,w,h,dx,dy,ticksPerFrame;
		string delimeter=",";
		bool parsing=true;
		string tetrada,curr_string=line;
		//cout<<"String is "<<curr_string;
		while(parsing){
			tetrada=curr_string.substr(0,curr_string.find(delimeter));		//1-2-3-4
			curr_string=curr_string.substr(curr_string.find(delimeter)+1,curr_string.length());
			x=tetrada.substr(0,tetrada.find("/"));
			tetrada=tetrada.substr(tetrada.find("/")+1,tetrada.length());
			y=tetrada.substr(0,tetrada.find("/"));
			tetrada=tetrada.substr(tetrada.find("/")+1,tetrada.length());
			w=tetrada.substr(0,tetrada.find("/"));
			tetrada=tetrada.substr(tetrada.find("/")+1,tetrada.length());
			h=tetrada.substr(0,tetrada.find("/"));
			tetrada=tetrada.substr(tetrada.find("/")+1,tetrada.length());
			dx=tetrada.substr(0,tetrada.find("/"));
			tetrada=tetrada.substr(tetrada.find("/")+1,tetrada.length());
			dy=tetrada.substr(0,tetrada.find("/"));
			tetrada=tetrada.substr(tetrada.find("/")+1,tetrada.length());
			ticksPerFrame=tetrada.substr(0,tetrada.find("/"));


			if(x.compare("0")==0)
				break;
			bounds.push_back(frame(stoi(x),stoi(y),stoi(w),stoi(h),stoi(dx),stoi(dy),stoi(ticksPerFrame)));
	}

		currFrame=0;
		frameSize=bounds.size();


	};

	frame getNextFrame(){
		unsigned tmpFrame = currFrame;
		currFrame = (++currFrame)%frameSize;
		//cout<<"Curr frame["<<currFrame<<"]	Frame size["<<frameSize<<"]\n";
		//cout<<"X:"<<currFrame.x<<" Y:"<<y<<" W:"<<width<<" H:"<<height;
		//bounds[tmpFrame].toString();
		return bounds[tmpFrame];
	}

	unsigned getCurrFrame(){
		return currFrame;
	}



	void toString(){
		std::cout << "First element of vector is" << bounds.front().x <<","<< bounds.front().y <<","<< bounds.front().height <<","<< bounds.front().width ;
	}


};


class AnimationHolder{
private:
	//AnimationFilm tmpFilm();
	string fighter;
	std::map<std::string,AnimationFilm> animationMap;

public:
	AnimationFilm tmpfilm();
	//AnimationFilm *tmpFiml=new Animation();
	AnimationHolder(string player){
		fighter=player;
	};

	void init(){
		string filename="animation_"+fighter+".txt";
		 std::ifstream file(filename);
		 std::string str;
		  getline(file,str);//remove infos
		  while(std::getline(file, str)){
			  string delimeter=",";
			  boolean parsing=true;
			  string tetrada,curr_string;
			  string move=str.substr(0,str.find(delimeter)); //move to map;
			  string frames=str.substr(str.find(delimeter)+1,str.length()); //frames to animationfilm
			  AnimationFilm frame_to_map(frames);
			  animationMap.insert(pair<string,AnimationFilm>(move,frame_to_map));
			  //animationMap.find("move")->second;
			}		//
		//String move=""; //to pernw apo csv
		//AnimationFilm temp(move,fighter);
		//map move_to_fighter
	}

	AnimationFilm getFilm(string move){
		return animationMap.find(move)->second;
	}


	//AnimationFilm getFilm(){
		//return tmpfilm;
	//}
};

#endif
