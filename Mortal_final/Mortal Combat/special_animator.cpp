#ifndef __ANIMATOR_H__
#define __ANIMATOR_H__

#include "animation.h"
#include "sprite.h"
#include <iostream>
#include "player.h"
#include <vector>
#include <string.h>
#include <map>
#include <sstream>

enum animationState{
	ANIMATION_FINISHED = 0,
	ANIMATION_RUNNING = 1,
	ANIMATION_STOPPED = 2
};


class Animator{
private:
	int cnt;
	string fighter;
	Player *player;
	animationState state;
	AnimationFilm film;
	int clockTicksForAnimation;
	AnimationHolder* holder;
	sprite *s;
	unsigned frX, frY, frW, frH, posX, posY;
	ALLEGRO_BITMAP * bm;
	int directionX;
	int directionY;
	int fr_nmbr;
	bool change_move_direction;
	bool is_jumping;
	bool canBeInterupted;
	map<string, action> actionMap;
	int testcounter;
public:
	Animator(sprite* spr,string name,Player *pl){
		s=spr;
		fighter=name;
		player=pl;
		cnt=0;
		state = ANIMATION_FINISHED;
		clockTicksForAnimation = 0;
		bm = s->getBM();
		directionX = 5;
		testcounter = 0;
		canBeInterupted = false;
		change_move_direction=false;
		is_jumping=false;
		directionY=0;
		holder = new AnimationHolder(name);
		holder->init();
		stopAction();
	};

	bool startAction(string action){
		if(action.compare("RightGettingHitPunch")==0 || action.compare("RightGettingHitHighKick")==0 || action.compare("RightGettingHitHighPunch")==0 || action.compare("RightGettingHitKick")==0 || action.compare("RightGettingHitUppercut")==0 || action.compare("RightGettingUpDownKnockout")==0||action.compare("RightGettingHit")==0 || action.compare("RightGettingHitLow")==0
		||action.compare("LeftGettingHitPunch")==0 || action.compare("LeftGettingHitHighKick")==0 || action.compare("LeftGettingHitHighPunch")==0 || action.compare("LeftGettingHitKick")==0 || action.compare("LeftGettingHitUppercut")==0 || action.compare("LeftGettingUpDownKnockout")==0||action.compare("LeftGettingHit")==0 || action.compare("LeftGettingHitLow")==0){
			cout<<"Invurnability Entered:)"<<endl;
			player->setInvurnability(true);
		}
		if(action=="Victory")
			cnt++;
		/*if(action.compare("LeftMiddleRightPunch") == 0){
			testcounter++;
		}*/
		if(state == ANIMATION_FINISHED ||
			canBeInterupted ||
			action.compare("LeftSweep") == 0 ||
			action.compare("RightSweep") == 0 ||
			action.compare("LeftIddleStart")==0||action.compare("RightIddleStart")==0){
			canBeInterupted = false;
			state = ANIMATION_RUNNING;

			if(cnt>0)
				film=holder->getFilm("VictoryFinal");
			/*else if(testcounter > 1){
				film=holder -> getFilm("LeftMiddleDoublePunch");
				testcounter = 0;
			}*/

			else
				film=holder->getFilm(action);
			clockTicksForAnimation = 0;
			return true;
		}
		return false;
	}

	void stopAction(){
		player->setInvurnability(false);
		if(!player->isAlive()){
			if(player->getLives()>0){
				if(player->getDirection().compare("right")==0){
					film=holder->getFilm("RightGettingHitUppercutTmp");
				}else{
					film=holder->getFilm("LeftGettingHitUppercutTmp");
				}
			}else{
				if(player->getDirection().compare("right")==0){
					film=holder->getFilm("RightFrozenDeadTmp");
				}else{
					film=holder->getFilm("LeftFrozenDeadTmp");
				}
			}
			//canBeInterupted=true;
			return;
		}
		string side=player->getDirection();
		directionX = 0;
		directionY = 0;
		state = ANIMATION_FINISHED;
		if(side=="left")
			
			film = holder -> getFilm("LeftIddle");
		else{
			film = holder -> getFilm("RightIddle");
		}
	}
	void testAction(){
		canBeInterupted=true;
		startAction("LeftBrutality");
		//maxClockTicksForAnimation = 50; //DEBUGGING DONT ERASE

	}

	void reroll(){
		cnt=0;
		if(player->getDirection().compare("right")==0){
			startAction("RightIddleStart");
		}else{
			startAction("LeftIddleStart");
		}
	}

	void draw(){

		if(--clockTicksForAnimation <=0){
			frame fr=film.getNextFrame();
			frX = fr.x;
			frY = fr.y;
			frW = fr.width;
			frH = fr.height;
			clockTicksForAnimation = fr.ticksPerFrame;
			s->positionX += fr.dx;
			s->positionY += fr.dy;
		}
		al_draw_bitmap_region(bm, frX, frY, frW, frH, s->positionX, s->positionY, 0);

		if(state == ANIMATION_RUNNING){
			if(film.getCurrFrame() == 0){
				//string side=
				stopAction();
			}
			//clockTicksForAnimation = 5; // MONO GIA DEBUGGING TWN EIKONWN, KANEI PAUSE KAI VLEPW POIES DEN EINAI KOMMENES KALA.
		}
	}
	void toString(){
		std::cout<<"This is a test\n";
		//myfilm.toString();
	}



};