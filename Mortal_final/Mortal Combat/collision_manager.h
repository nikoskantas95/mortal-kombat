#include <iostream>
#include <vector>
#include <string>
#include "sprite.h"
#include "player.h"
using namespace std;

class CollisionManager{

private:
	Player *player1,*player2;
	boolean can_calculate;

public:
	CollisionManager(Player *p1,Player *p2){
		player1=p1;
		player2=p2;
		can_calculate=true;
	}

	boolean checkCollision(){
		vector <int> h1,h2;
		//hitbox=<x,y,width,height>
		h1=player1->getHitbox();
		h2=player2->getHitbox();
		int collision_limit=abs(h1.at(0)-h2.at(0));
		//cout<<"Dif is "<<collision_limit<<endl;
		if(collision_limit <= 35)
			return true;
		else
			return false;
	}

	void DamageCalculation(){
		int dmg1,dmg2;
		if(!player1->isInvulnerable()){
			if(player1->isBlocking())
				dmg2=player2->getDamageDealed()/2;
			else
				dmg2=player2->getDamageDealed();
		}
		else{
			dmg2=0;
		}
		if(!player2->isInvulnerable()){
		if(player2->isBlocking())
			dmg1=player1->getDamageDealed()/2;
		else
			dmg1=player1->getDamageDealed();
		}
		else{
			dmg1=0;
		}


		player1->DecrementHealth(dmg2);
		player2->DecrementHealth(dmg1);

		cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl<<
		"p1 hit for : "<<player1->getDamageDealed()<<endl<<
			"p2 hit for : " <<player2->getDamageDealed()<<endl
		<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;


		cout<<"###########################################"<<endl<<
			"Player 1 health points: "<<player1->getHealth()<<" Lives: "<<player1->getLives()<<endl<<
			"Player 2 health points: "<<player2->getHealth()<<" Lives: "<<player2->getLives()<<endl
			<<"###########################################"<<endl;

	}


	int checkSPCollision(){
		vector <int> sh1,sh2;	//specials hitboxes
		int i=0;
		sh1=player1->getSpecial()->getHitbox();
		sh2=player2->getSpecial()->getHitbox();
		//cout<<player2->getPlayerHeigth()<<"---->"<<sh1.at(1)<<endl;

		for(i=0; i<60; i++){
			if(player1->getPlayerX()==sh2.at(0)-i||player1->getPlayerX()==sh2.at(0)+i ){
				cout<<"Special 2 hit!"<<endl<<
				"Player: "<<player2->getPlayerX()<<","<<player2->getPlayerHeigth()
				<<"Special: "<<sh1.at(0)<<","<<sh1.at(1)<<endl;
				return 0;
			}
		}
		if(player2->getPlayerX()==sh1.at(0)){
			cout<<"Special 1 hit!"<<endl<<
			"Player: "<<player2->getPlayerX()<<","<<player2->getPlayerHeigth()
			<<"Special: "<<sh1.at(0)<<","<<sh1.at(1)<<endl;
			return 1;
		}

			return 2;

	}



	/* !!!!!!!!!!!!!!!!!!!!!!!!
		What happens in the rare case where
		both players die at the same time?
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	*/
	boolean checkWinner(){
	if(player1->getHealth()==0 || player2->getHealth()==0)
		return true;
	else
	return false;
	}

	 /*	returns the last round's winner */
	void calculateWinner(){
		if(player1->getHealth()==0 && player2->getHealth()!=0){
			player1->DecrementLives();
			player2->setWinner(true);

		}
		if(player2->getHealth()==0 && player1->getHealth()!=0){
			player2->DecrementLives();
			player1->setWinner(true);

		}

	}


	void checkDirection(){
		if(player1->getPlayerX()> player2->getPlayerX()){
			player1->setDirection("right");
			player2->setDirection("left");
		}
		else if(player1->getPlayerX()< player2->getPlayerX()){
			player2->setDirection("right");
			player1->setDirection("left");
		}
	}




	void toString(){
		cout<<"Player1:\n";
		player1->toString();
		cout<<endl;

		cout<<"Player2:\n";
		player2->toString();
		cout<<endl;


	}

	void toggleDmgCalculation(){
		if(can_calculate)
			can_calculate=false;
		else
			can_calculate=true;
	}

	boolean canCalculate(){
		return can_calculate;
	}


};
