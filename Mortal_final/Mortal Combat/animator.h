#ifndef __ANIMATOR_H__
#define __ANIMATOR_H__

#include "animation.h"
#include "sprite.h"
#include <iostream>
#include "player.h"
#include <vector>
#include <string.h>
#include <map>
#include <sstream>


using namespace std;

enum animationState{
	ANIMATION_FINISHED = 0,
	ANIMATION_RUNNING = 1,
	ANIMATION_STOPPED = 2
};

typedef struct action{
	int dirx;
	int diry;
	int ticksPerFrame;
}action;

class Animator{
private:
	int cnt;
	string fighter;
	Player *player;
	animationState state;
	AnimationFilm film;
	int clockTicksForAnimation;
	AnimationHolder* holder;
	sprite *s;
	unsigned frX, frY, frW, frH, posX, posY;
	ALLEGRO_BITMAP * bm;
	int directionX;
	int directionY;
	int fr_nmbr;
	int xPullDestination;
	bool gettingPulled;
	bool pullingFlag;
	bool change_move_direction;
	bool is_jumping;
	bool canBeInterupted;
	map<string, action> actionMap;
	int testcounter;
public:
	Animator(sprite* spr,string name,Player *pl){
		s=spr;
		fighter=name;
		player=pl;
		cnt=0;
		gettingPulled=false;
		xPullDestination=0;
		state = ANIMATION_FINISHED;
		clockTicksForAnimation = 0;
		bm = s->getBM();
		directionX = 5;
		testcounter = 0;
		pullingFlag=false;
		canBeInterupted = false;
		change_move_direction=false;
		is_jumping=false;
		directionY=0;
		holder = new AnimationHolder(name);
		holder->init();
		stopAction();
	};

	bool startAction(string action){
		if(action.compare("LeftScorpionPull")==0 || action.compare("RightScorpionPull")==0 ){gettingPulled=true;}
		if(action.compare("RightGettingHitPunch")==0 || action.compare("RightGettingHitHighKick")==0 || action.compare("RightGettingHitHighPunch")==0 || action.compare("RightGettingHitKick")==0 || action.compare("RightGettingHitUppercut")==0 || action.compare("RightGettingUpDownKnockout")==0||action.compare("RightGettingHit")==0 || action.compare("RightGettingHitLow")==0
		||action.compare("LeftGettingHitPunch")==0 || action.compare("LeftGettingHitHighKick")==0 || action.compare("LeftGettingHitHighPunch")==0 || action.compare("LeftGettingHitKick")==0 || action.compare("LeftGettingHitUppercut")==0 || action.compare("LeftGettingUpDownKnockout")==0||action.compare("LeftGettingHit")==0 || action.compare("LeftGettingHitLow")==0){
		//	cout<<"Invurnability Entered:)"<<endl;
			player->setInvurnability(true);
		}
		if(action=="Victory")
			cnt++;
		/*if(action.compare("LeftMiddleRightPunch") == 0){
			testcounter++;
		}*/
		if(state == ANIMATION_FINISHED ||
			canBeInterupted || action.compare("RightDead")==0 ||action.compare("RightFrozen")==0 ||  action.compare("LeftDead")==0 ||action.compare("LeftFrozen")==0 ||
			action.compare("LeftSweep") == 0 ||
			action.compare("RightSweep") == 0 ||
			action.compare("LeftIddleStart")==0||action.compare("RightIddleStart")==0){
			canBeInterupted = false;
			state = ANIMATION_RUNNING;

			if(cnt>0){
				film=holder->getFilm("VictoryFinal");
			}
			/*else if(testcounter > 1){
				film=holder -> getFilm("LeftMiddleDoublePunch");
				testcounter = 0;
			}*/
			else{
			//	if(action.compare("VictoryFinal")==0)
				//	cout<<"!!"<<endl;
				film=holder->getFilm(action);
			}
			clockTicksForAnimation = 0;
			return true;
		}
		return false;
	}

	void setPullDestination(Player *enemy){
		if(player->getDirection().compare("left")==0){
			xPullDestination=(int)(enemy->getPlayerX()-50);
		}else{
			xPullDestination=(int)(enemy->getPlayerX()+50);
		}
	}

	void stopAction(){
		gettingPulled=false;
		player->setInvurnability(false);
		if(!player->isAlive()){
			if(player->getLives()>0){
				if(player->getDirection().compare("right")==0){
					film=holder->getFilm("RightGettingHitUppercutTmp");
				}else{
					film=holder->getFilm("LeftGettingHitUppercutTmp");
				}
			}else{
				if(player->getDirection().compare("right")==0){
					film=holder->getFilm("Fatality");
					//canBeInterupted = false;
					//state = ANIMATION_RUNNING;
					//canBeInterupted=true;
				}else{
					film=holder->getFilm("Fatality");
				}
			}
			//canBeInterupted=true;
			return;
		}
		string side=player->getDirection();
		directionX = 0;
		directionY = 0;
		state = ANIMATION_FINISHED;

	/*	if(player->isStunned()){
			film=holder->getFilm("VictoryFinal");
			return;
		}*/

		if(side=="left"){
			film = holder -> getFilm("LeftIddle");
		}
		else{
			film = holder -> getFilm("RightIddle");
		}
	}

	void testAction(string str){
		startAction(str);
	}

	void finalAction(string act){
		//canBeInterupted=true;
		//cout<<"!!!"<<endl;
		cnt=0;
		startAction(act);
		//maxClockTicksForAnimation = 50; //DEBUGGING DONT ERASE
	}

	



	void reroll(){
		cnt=0;
		if(player->getDirection().compare("right")==0){
			startAction("RightIddleStart");
		}else{
			startAction("LeftIddleStart");
		}
	}

	void draw(){
		if(gettingPulled==true){
			if(--clockTicksForAnimation <=0){
			frame fr=film.getNextFrame();
			frX = fr.x;
			frY = fr.y;
			frW = fr.width;
			frH = fr.height;
			clockTicksForAnimation = fr.ticksPerFrame;
			//if(clockTicks){}
			if(pullingFlag){ 
				s->positionX=xPullDestination;
			}
			if(!pullingFlag){pullingFlag=true;}
			}
			//if(clockTicksForAnimation==0){startAction("");}
	
		al_draw_bitmap_region(bm, frX, frY, frW, frH, s->positionX, s->positionY, 0);

		if(state == ANIMATION_RUNNING){
			if(film.getCurrFrame() == 0){
				//string side=
				if(player->isStunned()){
					startAction("Fatality");
				}else{
					stopAction();
				}
				//gettingPulled=false;
				//stopAction();
				//startAction("Fatality");
			}
			//clockTicksForAnimation = 5; // MONO GIA DEBUGGING TWN EIKONWN, KANEI PAUSE KAI VLEPW POIES DEN EINAI KOMMENES KALA.
		}
		
		
		
		}else{
		if(--clockTicksForAnimation <=0){
			frame fr=film.getNextFrame();
			frX = fr.x;
			frY = fr.y;
			frW = fr.width;
			frH = fr.height;
			clockTicksForAnimation = fr.ticksPerFrame;
			if(player->isStunned())
					return;

			if(player->getDirection().compare("left")==0){
				if(s->positionX <10){
					s->positionX=10;
					s->positionY += fr.dy;
				}
				else{
					s->positionX += fr.dx;
					s->positionY += fr.dy;
				}
		}
		else{
				if(s->positionX >735){
					s->positionX=735;
					s->positionY += fr.dy;
				}
				else{
					s->positionX += fr.dx;
					s->positionY += fr.dy;
				}
			}

	}
		al_draw_bitmap_region(bm, frX, frY, frW, frH, s->positionX, s->positionY, 0);

		if(state == ANIMATION_RUNNING){
			if(film.getCurrFrame() == 0){
				//string side=
				stopAction();
			}
			//clockTicksForAnimation = 5; // MONO GIA DEBUGGING TWN EIKONWN, KANEI PAUSE KAI VLEPW POIES DEN EINAI KOMMENES KALA.
		}
		
		}
	}
	void toString(){
		std::cout<<"This is a test\n";
		//myfilm.toString();
	}



};


class GameAnimator{
private:
	sprite *s;
	string file;
	Player *player;
	std::map<std::string,frame> hp_map;
	int lifeX;

	/*!!!!!! paulakis stuff dn 3erw ti kanoun !!!!!*/
	unsigned frX, frY, frW, frH, posX, posY;
	ALLEGRO_BITMAP * bm,*lifeBM;
	int directionX;
	int directionY;
	int fr_nmbr;
	int clockTicksForAnimation;


public:
	GameAnimator(sprite* spr,string args,Player *pl){
		s=spr;
		file=args;
		player=pl;
		bm = s->getBM();
		if(pl->getName().compare("p1")==0){
			lifeX=30;
		}
		else{
			lifeX=700;
		}

	}

	void init(){
	//	cout<<"Searching for file: "<<file<<endl;
		std::ifstream file(file);
		std::string str;
		 while(std::getline(file, str)){
			 	string delimeter=",";
			  string hp_name=str.substr(0,str.find(delimeter));
				string tmp=str.substr(str.find(delimeter)+1,str.length());
				string frame_info=tmp.substr(0,tmp.find(delimeter));
				frame new_frame(frame_info);
			//	new_frame.toString();
				hp_map.insert(pair<string,frame>(hp_name,new_frame));
				lifeBM= al_load_bitmap("more_staff.png");
				al_convert_mask_to_alpha(lifeBM, al_map_rgb(255, 1, 255));

		 }
	}

	frame getHealthFrame(int health){
		string aux =player->getName();
		aux+="_";
		//aux+=health;
	/* health to string */
		std::string int_to_str;
		std::stringstream out;
		out << health;
		int_to_str = out.str();
		// eg. p1_100
		aux+=int_to_str;
		return hp_map.find(aux)->second;
		//fr.toString();
		//cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl<<
	//	fr.toString()<<endl;
		//string find=player->getName()+to_string(10);
			//return hp_map.find(player->getName()+"");

	}



	void draw(){
			//frame fr=getHealthFrame(player->getHealth());
			frame fr=getHealthFrame(player->getHealth());
			//frame fr(4,125,84,6,1,1,1);


		if(--clockTicksForAnimation <=0){

			frX = fr.x;
			frY = fr.y;
			frW = fr.width;
			frH = fr.height;
			clockTicksForAnimation = fr.ticksPerFrame;
			s->positionX += fr.dx;
			s->positionY += fr.dy;
		}
		al_draw_bitmap_region(bm, frX, frY, frW, frH, s->positionX, s->positionY, 0);
		if(player -> getLives() == 2){
				al_draw_bitmap_region(lifeBM, 690, 700, 17, 17, lifeX, 15, 0);
				al_draw_bitmap_region(lifeBM, 690, 700, 17, 17, lifeX+20, 15, 0);
			}
		else if(player -> getLives() == 1){
				al_draw_bitmap_region(lifeBM, 690, 700, 17, 17, lifeX, 15, 0);
			}
			//clockTicksForAnimation = 5; // MONO GIA DEBUGGING TWN EIKONWN, KANEI PAUSE KAI VLEPW POIES DEN EINAI KOMMENES KALA.
		}


};

class SpMoveAnimator{

private:
	int cnt;
	string fighter;
	Player *player;
	animationState state;
	AnimationFilm film;
	int clockTicksForAnimation;
	AnimationHolder* holder;
	frame fr;
	sprite *s;
	unsigned frX, frY, frW, frH, posX, posY;
	ALLEGRO_BITMAP * bm;
	int directionX;
	int directionY;
	int hit;
	int spear_long;
	
	int fr_nmbr;
	bool change_move_direction;
	bool is_jumping;
	bool started;
	//boolean spear;
	bool canBeInterupted;
	boolean spear;
	map<string, action> actionMap;
	int testcounter;

public:
	SpMoveAnimator(sprite* spr,string name,Player *pl){
		s=spr;
		fighter=name;
		player=pl;
		cnt=0;
		spear_long=0;
		hit=0;
		state = ANIMATION_FINISHED;
		started=false;
		clockTicksForAnimation = -1;
		spear_long=50;
		bm = s->getBM();
		directionX = 5;
		testcounter = 0;
		canBeInterupted = true;
		change_move_direction=false;
		is_jumping=false;
		directionY=0;
		holder = new AnimationHolder(name);
		spear=false;
		holder->init();
		stopAction();
	}

	void startAction(){
			canBeInterupted = true;
			hit=0;
			state = ANIMATION_RUNNING;
			if(player->getName().compare("p1")==0){
				cout<<"Special subzero"<<endl;
				if(player->getDirection()=="left"){
					s->positionX = player->getPlayerX()+10;
					s->positionY = 60;
					film=holder->getFilm("LeftFreezeBallTR");
				}else{
					s->positionX = player->getPlayerX()-10;
					s->positionY = 60;
					film=holder->getFilm("RightFreezeBallTR");
				}
		}
		else{
			cout<<"Special scorpion"<<endl;
			spear_long=50;
			if(player->getDirection()=="left"){
				s->positionX = player->getPlayerX()+10;
				s->positionY = 120;
				film=holder->getFilm("LeftSpear");
			}else{
				s->positionY = 120;
				s->positionX = player->getPlayerX()-10;
				film=holder->getFilm("RightSpear");
			}
		}
			fr=film.getNextFrame();
			clockTicksForAnimation = 0;
			cnt=0;
			started=true;

	}

	void startAction(string colision_anime){
			canBeInterupted = true;
			hit=1;
			state = ANIMATION_RUNNING;
			if(player->getDirection()=="left"){
				//s->positionX = player->getPlayerX()+10;
				film=holder->getFilm("LeftFreezeBallHIT");
			}else{
				//s->positionX = player->getPlayerX()-10;
				film=holder->getFilm("RightFreezeBallHIT");
			}
			//s->positionY = 60;

			//fr=film.getNextFrame();
			clockTicksForAnimation = 0;
			cnt=0;
			started=true;
	}

	void stopAction(){
	string side=player->getDirection();
	directionX = 0;
	directionY = 0;
	hit=0;
	cnt=0;
	s->positionX = -1000;
	s->positionY = -1000;
	started=false;
	spear=false;
	state = ANIMATION_FINISHED;
}

	void draw(){
		if(hit==1){
			//cout <<"##"<<clockTicksForAnimation<<"##"<<endl;

			if(clockTicksForAnimation<0){
				frame fr=film.getNextFrame();
				frX = fr.x;
				frY = fr.y;
				frW = fr.width;
				frH = fr.height;
				clockTicksForAnimation = fr.ticksPerFrame;
			//	cout <<"["<<clockTicksForAnimation<<"]"<<endl;
				s->positionX += fr.dx;
				s->positionY += fr.dy;
			}else{
				stopAction();
			}
			al_draw_bitmap_region(bm, frX, frY, frW, frH, s->positionX, s->positionY, 0);

		}
		else if(started){
			//cnt++;
			if(player->getName().compare("p2")==0){
			if(--clockTicksForAnimation<=0){
			spear_long+=15;
			fr=film.getNextFrame();
			frX = fr.x;
			frY = fr.y;
			frW = spear_long;
			frH = fr.height;
			clockTicksForAnimation = fr.ticksPerFrame;
			if(player->getDirection().compare("left")==0){
				s->positionX += 15;
			}else{
				s->positionX -= 15;
			}
			s->positionY += fr.dy;
			}
			al_draw_bitmap_region(bm, frX, frY, frW, frH, s->positionX, s->positionY, 0);
			if(player->getDirection().compare("left")==0 && spear_long>150){
				cout<<"Spearrr longgggggg"<<spear_long<<endl;
				spear_long=50;
				stopAction();
			}else if(spear_long>200){
				cout<<"Spearrr longgggggg"<<spear_long<<endl;
				spear_long=50;
				stopAction();

				}
			}
			else{
			cnt++;
			frX = fr.x;
			frY = fr.y;
			frW = fr.width;
			frH = fr.height;
			clockTicksForAnimation = fr.ticksPerFrame;
			if(player->getDirection()=="left"){
				s->positionX += 5;
			}else{
				s->positionX -= 5;
			}
			s->positionY += fr.dy;
			al_draw_bitmap_region(bm, frX, frY, frW, frH, s->positionX, s->positionY, 0);
			if(cnt==200){
				//cout<<"STOP"<<endl;
				stopAction();
			}
		}
			//clockTicksForAnimation = 5; // MONO GIA DEBUGGING TWN EIKONWN, KANEI PAUSE KAI VLEPW POIES DEN EINAI KOMMENES KALA.

		}

	}

};


#endif
