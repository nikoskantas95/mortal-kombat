#ifndef __GAME_H_
#define __GAME_H_
#include <iostream>
#include <vector>
#include <string>
#include "sprite.h"
#include "player.h"

class Game{

private:
	Player *p1,*p2;
	sprite *sp1,*sp2;
	int round;
	boolean start_new_round;


public:
	Game(Player *player1,sprite *sprite1,Player *player2,sprite *sprite2){
		p1=player1;
		p2=player2;
		sp1=sprite1;
		sp2=sprite2;
		round=0;
		start_new_round=false;
	}

	void setNewRound(){
		p1->fullHeal();
		sp1->positionX=100;
		sp1->positionY=100;
		p1->updatePlayerPosition();
		p2->fullHeal();
		sp2->positionX=600;
		sp2->positionY=100;
		p2->updatePlayerPosition();
		round++;
		toggleNewRound();
	}

	int getRound(){
		return round;
	}

	boolean hasEnded(){
	//	cout<<"P1: "<<p1->getLives()<<"	P2: "<<p2->getLives()<<endl;
		if(p1->hasLives()==true && p2->hasLives()==true){
			//cout <<"FALSE"<<endl;
			return false;
		}
		else{
			//cout<<"TRUE"<<endl;
				return true;
			}
	}

	string getWinner(){
			if(p1->hasLives())
				return p1->getName();
			else
				return p2->getName();
	}

	void newGame(){
		p1->newPlayer();
		p2->newPlayer();
		sp1->newSprite(p1->getName());
		sp2->newSprite(p2->getName());
	}




	void toggleNewRound(){
		if(start_new_round)
			start_new_round=false;
		else
			start_new_round=true;
	}


	boolean starNewRound(){
		return start_new_round;
	}

	void executeFatality(Player *winner,Animator *win_anime,Player *loser,Animator *los_anime){
	//	los_anime->testAction("RightFrozen");
		if(winner->getDirection().compare("left")==0){
			win_anime->finalAction("LeftBrutality");
		}
		else{
				win_anime->finalAction("RightBrutality");
		}
		if(loser->getDirection().compare("right")==0){

			los_anime->finalAction("RightDead");
		}
			else{
				los_anime->startAction("LeftDead");			
			}
		//los_anime->testAction("RightDead");

	}

};

#endif
