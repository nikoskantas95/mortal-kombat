#include "arena.h"
#include "sprite.h"
#include "animator.h"
#include "animation.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <Windows.h>
#include <time.h>
#include <allegro5\allegro_primitives.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>
#include <allegro5\allegro_native_dialog.h>
#include <allegro5\allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include "input_manager.h"
#include "player.h"
#include "collision_manager.h"
#include "game.h"
#include "special_move.h"

//###############################
// Defines
//###############################
enum SOUND{START,FIGHT,HIT,PAIN,SUBZERO_WIN,SCORPION_WIN,FREEZE,SPEAR,FATALITY_BEGIN,FINISH};
#define STAGE_WIDTH 776
#define STAGE_HEIGHT 240
#define MENU_WIDTH 593
#define MENU_HEIGHT 599
#define FPS 65
#define STAGE "TEMPLE_SHRINE_FINAL_HD.jpg"
#define MENU "menu.png"

//enum DIRECTION{RIGHT, LEFT, UP, DOWN, LCTRL, ALT, LSHIFT, CAPSLOCK, TAB, A, B, C, D, E, F, G, H, I, J, K};


int main(void){
	bool render = true;
	bool at_menu=true;
	bool play_menu_sound=true;
	ALLEGRO_DISPLAY* display = NULL;
	//ALLEGRO_SAMPLE *sample=NULL,*hit=NULL;
//	ALLEGRO_SAMPLE_ID sample_id;
	ALLEGRO_BITMAP* bgImage = NULL,*menuImage=NULL;
	ALLEGRO_BITMAP* subzeroBM = NULL,*hpBM;
	ALLEGRO_BITMAP* scorpionBM = NULL;
	ALLEGRO_EVENT_QUEUE* event_queue = NULL;
	ALLEGRO_TIMER* timer;
	int timer_count=0;
	int sound_effect;



	boolean done = false;

	if(!al_init()){
		fprintf(stderr, "failed to init allegro\n");
		return -1;
	}
	al_set_new_display_flags(ALLEGRO_RESIZABLE);
	//display = al_create_display(MENU_WIDTH, MENU_HEIGHT);
	display = al_create_display(MENU_WIDTH, MENU_HEIGHT);
	al_set_window_title(display, "MORTAL KOMBAT");

	if(!display){
		fprintf(stderr, "failed to create display\n");
		return -1;
	}

	//#################
	// ADDON INSTALL
	//#################
	al_init_primitives_addon();
	al_install_keyboard();
	al_init_image_addon();
	al_init_font_addon();
	al_init_ttf_addon();
	al_install_audio();
	al_init_acodec_addon();
	al_reserve_samples(10);
//	al_install_audio();
	//al_init_acodec_addon();
	//al_reserve_samples(3);




	//################
	//PROJECT INIT
	//################
	event_queue = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS); //60FPS

	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_timer_event_source(timer));


	bgImage = al_load_bitmap("arena.png");
	menuImage=al_load_bitmap("menu.png");
	subzeroBM = al_load_bitmap("subzero.png");
	scorpionBM = al_load_bitmap("scorpion.png");
	hpBM= al_load_bitmap("health_sprites.png");
	al_convert_mask_to_alpha(subzeroBM, al_map_rgb(255, 255, 255));
	al_convert_mask_to_alpha(scorpionBM, al_map_rgb(255, 255, 255));
	al_convert_mask_to_alpha(hpBM, al_map_rgb(0, 255, 255));

	//###################
	// SPRITES INIT
	//##################

	//sprite subzero(subzeroBM, 20, 20, 42, 100, 300, 350);

	/*TEST 123 */

	sprite subzero(subzeroBM, 20, 20, 42, 100, 100, 100);
	//sprite subzero2(subzeroBM, 20, 20, 42, 100, 600, 100);
	sprite scorpion(scorpionBM, 20, 20, 42, 100, 600, 100);
	sprite subzero_sp(subzeroBM, 20, 20, 42, 100, 0, 0);
	sprite scorpion_sp(scorpionBM, 20, 20, 42, 100, 0, 0);
	sprite hp1(hpBM, 20, 20, 42, 100, 10, 5);
	sprite hp2(hpBM, 20, 20, 42, 100, 480, 5);
	Player player1("p1", &subzero, "left");
	Player player2("p2", &scorpion, "right");
	//Player player2("p2", &subzero2, "right");
	specialMove special1(&subzero_sp);
	specialMove special2(&scorpion_sp);
	player1.setSpecial(&special1);
	player2.setSpecial(&special2);
	Animator subzeroAnimator(&subzero,"subzero",&player1);
	Animator scorpionAnimator(&scorpion,"scorpion",&player2);
	//Animator scorpionAnimator(&subzero2,"subzero",&player2);
	SpMoveAnimator subzero_spAnimator(&subzero_sp,"subzero",&player1);
	SpMoveAnimator scorpion_spAnimator(&scorpion_sp,"scorpion",&player2);
	GameAnimator game_p1(&hp1,"animation_health.txt",&player1);
	GameAnimator game_p2(&hp2,"animation_health.txt",&player2);
	soundManager sound;
	inputManager inp_manager1(&player1,&sound);
	inputManager inp_manager2(&player2,&sound);
	CollisionManager collision_manager(&player1,&player2);
	Game game(&player1,&subzero,&player2,&scorpion);
	game_p1.init();
	game_p2.init();

	  //De vazoume tpt meta to timer ektos apo to game loop
	//###################
	// GAME LOOP
	//###################
	int dir1 = 666;
	int dir2 = 666;
	bool executed1=false;
	bool executed2=false;
	//test start////
	std::vector<string> keyBuffer1;
	std::vector<string> keyBuffer2;
	string dir_pressed;
	string dir_released;
	if(sound.canPlaySound(START)){
		sound.playSound(START);
	}
	al_start_timer(timer);
	while(!done){
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		if(ev.type == ALLEGRO_EVENT_KEY_DOWN){
			switch(ev.keyboard.keycode)
			{
			case ALLEGRO_KEY_TAB:
			//subzeroAnimator.testAction("LeftScorpionPull");
				//cout<<"TAB"<<endl;
				if(game.hasEnded()){
					sound.playSound(8);
					sound.playSound(9);
					if(player1.isWinner()){
						cout<<"Winner p1"<<endl;
						game.executeFatality(&player1,&subzeroAnimator,&player2,&scorpionAnimator);
					}
					else{
						cout<<"Winner p2"<<endl;
					game.executeFatality(&player2,&scorpionAnimator,&player1,&subzeroAnimator);

				}
			}
				break;
			case ALLEGRO_KEY_ESCAPE:
				done = true;
				break;
			case ALLEGRO_KEY_E:
				keyBuffer1.push_back("sp");
				break;
			case ALLEGRO_KEY_PAD_7:
				keyBuffer2.push_back("sp");
				break;
			case ALLEGRO_KEY_D:
				keyBuffer1.push_back("right_press");
				break;
			case ALLEGRO_KEY_A:
				keyBuffer1.push_back("left_press");
				break;
			case ALLEGRO_KEY_W:
				keyBuffer1.push_back("up_press");
				break;
			case ALLEGRO_KEY_S:
				keyBuffer1.push_back("down_press");
				break;
			case ALLEGRO_KEY_J:
				keyBuffer1.push_back("punch_press");
				break;
			case ALLEGRO_KEY_K:
				keyBuffer1.push_back("kick_press");
				break;
			case ALLEGRO_KEY_L:
				keyBuffer1.push_back("block_press");
				break;
			case ALLEGRO_KEY_Q:
				keyBuffer1.push_back("special_press");
				break;
			case ALLEGRO_KEY_I:
				keyBuffer1.push_back("high_punch_press");
				break;
			//other cases for scorpion
			case ALLEGRO_KEY_RIGHT:
				keyBuffer2.push_back("right_press");
				break;
			case ALLEGRO_KEY_LEFT:
				keyBuffer2.push_back("left_press");
				break;
			case ALLEGRO_KEY_UP:
				keyBuffer2.push_back("up_press");
				break;
			case ALLEGRO_KEY_DOWN:
				keyBuffer2.push_back("down_press");
				break;
			case ALLEGRO_KEY_PAD_4:
				keyBuffer2.push_back("punch_press");
				break;
			case ALLEGRO_KEY_PAD_5:
				keyBuffer2.push_back("kick_press");
				break;
			case ALLEGRO_KEY_PAD_6:
				keyBuffer2.push_back("block_press");
				break;
			case ALLEGRO_KEY_PAD_8:
				keyBuffer2.push_back("high_punch_press");
				break;
			case ALLEGRO_KEY_PAD_9:
				keyBuffer2.push_back("special_press");
				break;
			case ALLEGRO_KEY_ENTER:
			if(game.hasEnded())
				at_menu=true;
				if(!at_menu){
					if(game.starNewRound()){
						keyBuffer1.clear();
						keyBuffer2.clear();
						game.setNewRound();
						sound.setPlaySound(FIGHT,true);
						subzeroAnimator.reroll();
						scorpionAnimator.reroll();
						collision_manager.toggleDmgCalculation();

					}
					else{
						cout<<"Rounds must end first"<<endl;
					}
					break;
				}
				else{
					if(sound.canPlaySound(START)){
						sound.stopSound();
				}
					if(game.hasEnded()){
						game.setNewRound();
						subzeroAnimator.reroll();
						scorpionAnimator.reroll();
						collision_manager.toggleDmgCalculation();
						player1.revive();
						player2.revive();
						al_resize_display(display,MENU_WIDTH,MENU_HEIGHT);
						sound.resetSounds();
						sound.playSound(START);
						render=true;
					}
					else{
						al_resize_display(display,STAGE_WIDTH,STAGE_HEIGHT);
						render=false;
						at_menu=false;
						sound.setPlaySound(START,false);
					}
				}
			}
		}
		if(ev.type == ALLEGRO_EVENT_KEY_UP){
			switch(ev.keyboard.keycode)
			{
			case ALLEGRO_KEY_L:
				keyBuffer1.push_back("block_release");
				break;
			case ALLEGRO_KEY_PAD_6:
				keyBuffer2.push_back("block_release");
				break;
			case ALLEGRO_KEY_D:
				keyBuffer1.push_back("right_release");
				break;
			case ALLEGRO_KEY_A:
				keyBuffer1.push_back("left_release");
				break;
			case ALLEGRO_KEY_S:
				keyBuffer1.push_back("down_release");
				break;
			case ALLEGRO_KEY_RIGHT:
				keyBuffer2.push_back("right_release");
				break;
			case ALLEGRO_KEY_LEFT:
				keyBuffer2.push_back("left_release");
				break;
			case ALLEGRO_KEY_DOWN:
				keyBuffer2.push_back("down_release");
				break;

			}
		}

		if(ev.type == ALLEGRO_EVENT_TIMER &&	at_menu==false){
				special1.updateSpecialPosition();
				special2.updateSpecialPosition();
				int aux_col=collision_manager.checkSPCollision();
				switch(aux_col){
					case 0: //hit p1
					if(!player1.isStunned() && player1.isBlocking()==false){
						player1.setStunned(true);
						//scorpion_spAnimator.startAction("??");
						scorpion_spAnimator.stopAction();
						subzeroAnimator.setPullDestination(&player2);
						if(player1.getDirection().compare("left")==0)
							subzeroAnimator.startAction("LeftScorpionPull");
						else
							subzeroAnimator.startAction("RightScorpionPull");
				}
					break;
					case 1:	//hit p2
					
						subzero_spAnimator.startAction("ICEBALL_STOPS_HERE");
						if(!player2.isStunned() && player2.isBlocking()==false){
							sound.playSound(6);
							player2.setStunned(true);
							if(player2.getDirection().compare("left")==0)
								scorpionAnimator.startAction("LeftFrozen");
							else
								scorpionAnimator.startAction("RightFrozen");
					}
					break;

					default: //F
					break;
				}

			if(sound.canPlaySound(FIGHT)){
				cout<<"Fight"<<endl;
				sound.playSound(FIGHT);
				sound.setPlaySound(FIGHT,false);
			}
			if(timer_count==3){
				sound.setPlaySound(7,true);
				executed1=false;
				executed2=false;
				timer_count=0;
				dir1=inp_manager1.getInput(keyBuffer1);
				dir2=inp_manager2.getInput(keyBuffer2);
				player1.updatePlayerPosition();
				player2.updatePlayerPosition();

				executed1=inp_manager1.executeInput(&subzeroAnimator,&subzero_spAnimator,dir1,dir2,collision_manager.checkCollision());
				executed2=inp_manager2.executeInput(&scorpionAnimator,&scorpion_spAnimator,dir2,dir1,collision_manager.checkCollision());
				if(executed1)
					inp_manager1.playSound(dir1);
				if(executed2)
					inp_manager2.playSound(dir2);
					if(collision_manager.checkCollision()==true){
						collision_manager.DamageCalculation();
						if(executed1||executed2){
							inp_manager1.executeGotHit(&subzeroAnimator,dir2,dir1);
							inp_manager2.executeGotHit(&scorpionAnimator,dir1,dir2);
						}
					}

					collision_manager.checkDirection();
					keyBuffer1.clear();
					keyBuffer2.clear();
					if(collision_manager.checkWinner()){
						if(collision_manager.canCalculate()){
							collision_manager.toggleDmgCalculation();
							collision_manager.calculateWinner();
						}
					}
					if(player1.isWinner() || player2.isWinner())
						game.toggleNewRound();	//toggles enter to start a new round
					if(game.hasEnded()){
						//winner=game.getWinner();
						if(game.getWinner().compare("p1")==0){
							if(sound.canPlaySound(SUBZERO_WIN))
								sound.playSound(SUBZERO_WIN);

						}
						else{
							if(sound.canPlaySound(SCORPION_WIN))
								sound.playSound(SCORPION_WIN);
						}
					}
				}
			else{
				timer_count++;
			}
			render = true;
		}

		if(render){
			if(!at_menu){
				al_flip_display();
				al_draw_bitmap_region(bgImage, 48, 72, 776, 240, 0, 0, 0);
				subzeroAnimator.draw();
				subzero_spAnimator.draw();
				scorpionAnimator.draw();
				scorpion_spAnimator.draw();
				game_p1.draw();
				game_p2.draw();
				//sound.setPlaySound(2,true);
				render = false;
			}
			else{

				al_flip_display();
				al_draw_bitmap_region(menuImage, 0, 0, MENU_WIDTH, MENU_HEIGHT, 0, 0, 0);
			}
		}

////test end//////
	}

	al_destroy_timer(timer);
	//al_destroy_sample(sample);
	al_destroy_display(display);
	al_destroy_bitmap(bgImage);
	al_destroy_event_queue(event_queue);
	system("pause");
	return 0;
}
