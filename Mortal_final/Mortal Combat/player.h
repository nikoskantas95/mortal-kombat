#ifndef __PLAYER_H_
#define __PLAYER_H_
#include <iostream>
#include <vector>
#include <string>
#include "sprite.h"
#include "special_move.h"

using namespace std;
enum PLAYER_STATE{ON_GROUND,IN_AIR,DEAD};


class Player{

private:
	Player *enemy;
	int lives;
	int health;
	boolean invulnerable;
	boolean winner;
	string name;
	string direction;
	PLAYER_STATE state;
	int X,Y,Width,Height;
	vector <int> hitbox;//<x,y,width,height>
	int dmg_deal;
	boolean blocking;
	sprite* player_sprite;
	specialMove *special;
	boolean stunned;



public:

	Player(string str,sprite* spr,string dir){
		name=str;
		direction=dir;
		health=100;
		lives=2;
		state=ON_GROUND;
		player_sprite=spr;
		dmg_deal=0;
		winner=false;
		blocking=false;
		invulnerable=false;
		stunned=false;
		updatePlayerPosition();
	}

	boolean isStunned(){
		return stunned;
	}

	void setStunned(boolean arg){
		stunned=arg;
	}

	string setName(string str){
		name=str;
	}


	string getName(){
		return name;

	}

	vector <int> getHitbox(){
		return hitbox;
	}

	void updateHitbox(int x,int y,int width,int height){
		hitbox[0]+=x;
		hitbox[1]+=y;
		hitbox[2]+=width;
		hitbox[3]+=height;
		printHitbox();
	}

	void printHitbox(){
		cout<<"Hitbox player: "<<name<<"\n<"<<hitbox[0]<<","<<hitbox[1]<<","<<hitbox[2]<<","<<hitbox[3]<<">\n";
	}

	int getPlayerX(){
		return X;
	}

	int getPlayerY(){
		return Y;
	}
	int getPlayerWidth(){
		return Width;
	}
	int getPlayerHeigth(){
		return Height;
	}
	void setPlayerHeight(int val){
		Height= val;
	}
	string getDirection(){
		return direction;
	}

	void setDirection(string new_dir){
		direction=new_dir;
	}

	void setSpecial(specialMove *sp){
		special=sp;
	}

	specialMove* getSpecial(){
		return special;
	}

	int getHealth(){
		return health;
	}

	void DecrementHealth(int damage){
		if(health-damage>=0)
			health=health-damage;
		else
			health=0;
	}

	int getLives(){
		return lives;
	}

	void DecrementLives(){
		if(lives-1>=0)
			lives=lives-1;
		else
			lives=0;
	}

	void fullHeal(){
		setWinner(false);
		health=100;
	}

	void revive(){
		lives=2;
	}

	void newPlayer(){
			fullHeal();
			updatePlayerPosition();
		//	health=100;
			lives=1;
			state=ON_GROUND;
		//	player_sprite=spr;
			dmg_deal=0;
		//	winner=false;
			blocking=false;
			invulnerable=false;
			if(getName().compare("p1")==0)
				setDirection("left");
			else
				setDirection("right");
			updatePlayerPosition();

	}

	void updatePlayerPosition(){
		//cout<<"POS: "<<X<<","<<Y<<endl;
		X=player_sprite->positionX;
		Y=player_sprite->positionY;
		//cout<<"POS: "<<X<<","<<Y<<endl;
		Width=player_sprite->width;
		Height=player_sprite->height;
		hitbox.clear();
		hitbox.push_back(player_sprite->positionX);
		hitbox.push_back(player_sprite->positionY);
		hitbox.push_back(player_sprite->width);
		hitbox.push_back(player_sprite->height);
	}



	void setDamage(int damage){
		dmg_deal=damage;
	}

	int getDamageDealed(){
		return dmg_deal;
	}

	boolean isAlive(){
		if(health>0)
			return true;
		else
			return false;
	}

	boolean hasLives(){
		if(lives>0)
			return true;
		else
			return false;
	}

	void setWinner(boolean arg){
		winner=arg;
	}

	boolean isWinner(){
		return winner;
	}

	boolean isBlocking(){
		return blocking;
	}

	void setBlocking(boolean arg){
		blocking =arg;
	}

	boolean isInvulnerable(){
		return invulnerable;
	}

	void setInvurnability(boolean args){
		//cout<<"Invurnebality changed\n";
		invulnerable=args;
	}


	void toString(){
		cout<<name<<" Information\nCoordinates: "<<X<<","<<Y<<
			endl<<"Width,Heigth: "<<Width<<","<<Height<<endl
		<<"Health: "<<health;
	}

};



#endif
