#include <iostream>
#include <vector>
#include <cmath>
#include <Windows.h>
#include <time.h>
#include <string>
#include "arena.h"
#include "sprite.h"
#include "animator.h"
#include "animation.h"
#include "player.h"
#include "soundManager.h"
#include "special_move.h"
using namespace std;

//DOWN  DOWN_PUNCH DOWN_BLOCK DOWN_KICK STAY_DOWN STAY_DOWN_BLOCKING

enum MOVE{DOUBLE_PUNCH, RIGHT, LEFT, UP, DOWN, PUNCH, BLOCK/*block*/, KICK/*kick*/, DOWN_PUNCH/*uppercut*/, DOWN_BLOCK/*low block*/, DOWN_KICK/*low kick*/, SP_MOVE ,HIGH_KICK,HIGH_PUNCH,JumpRoll,GRAB,STAY_DOWN,STAY_BLOCKING,STAY_DOWN_BLOCKING, Sweep};
enum STATE{MOVING_RIGHT,MOVING_LEFT,NONE,DUCK,BLOCKING,DOWNBLOCKING};

class inputManager{
private:
	vector<string> localBuffer;
	Player *player;
//	specialMove *special_move;
	STATE mvState;
	int count;
	bool combo;
	bool special;
	int counter_duck;
	soundManager sound_manager;



public:

	inputManager(Player *pl,soundManager *sound){
		player=pl;
		//special_move=sp;
		//soundManager sound_manager("???");\
		sound_manager=sound;
		count=0;
		mvState=STATE::NONE;
		counter_duck=0;
	};

	bool makes_combo(string str){
		if(str.compare("high_punch_press")==0 ||
			//str.compare("punch_press")==0 ||
			str.compare("kick_press")==0||
			str.compare("block_press")==0 ||
			str.compare("right_press")==0 ||
			str.compare("left_press")==0||
			str.compare("down_press")==0  ){
			return false;
		}else{
			return true;
		}
	};

	bool make_combo_of_two(string str1,string str2){
		if(	 (str1.compare("punch_press")==0 && str2.compare("punch_press") == 0) ||
			 (str1.compare("right_press")==0 && str2.compare("kick_press") == 0) ||
			 (str1.compare("left_press")==0	 &&	str2.compare("kick_press")==0)	 ||
			 (str1.compare("up_press")==0	 &&	str2.compare("left_press")==0)	 ||
			 (str1.compare("down_press")==0	 &&	str2.compare("punch_press")==0)/*upper cut*/ ||
			 (str1.compare("down_press")==0  &&	str2.compare("kick_press")==0) /*low kick*/ ||
			 (str1.compare("down_press")==0  &&	str2.compare("block_press")==0)/*low block*/||
			 (str1.compare("up_press")==0	 &&	str2.compare("block_press")==0)/*grab*/||
			 (str1.compare("up_press")==0	 &&	str2.compare("punch_press")==0)	 ||
			 (str1.compare("up_press")==0	 &&	str2.compare("kick_press")==0)	 ||
			 (str1.compare("up_press")==0	 &&	str2.compare("right_press")==0)){
			return true;
		}else{
			return false;
		}
	}

	bool promising_special_move(string str1,string str2){
		if(str1.compare("up_press")==0 &&str2.compare("special_press")==0){/*W - Q - J:PLAYER A        UP - 9 - 4:Player B*/
			return true;
		}else{
			return false;
		}

	}

	// down_press down_up, right_press, right_up, punch press special
	// down, right, +X
	// punch + punch -> aristero de3i
	bool validate_special_move(string str1,string str2,string str3){
		if(str1.compare("up_press")==0 && str2.compare("special_press")==0 && str3.compare("punch_press")==0){/*W - Q - J:PLAYER A       UP- 9 - 4:Player B*/
			return true;
		}else{
			return false;
		}

	}

	int key_to_move(string str){
		if(str.compare("high_punch_press")==0){
			return MOVE::HIGH_PUNCH;
		}else if(str.compare("punch_press")==0){
			return MOVE::PUNCH;
		}else if(str.compare("kick_press")==0){
			return MOVE::KICK;
		}else if(str.compare("block_press")==0){
			return MOVE::BLOCK;
		}else if(str.compare("up_press")==0){
			return MOVE::UP;
		}else if(str.compare("down_press")==0){
			return MOVE::DOWN;
		}else if(str.compare("right_press")==0){
			return MOVE::RIGHT;
		}else if(str.compare("left_press")==0){
			return MOVE::LEFT;
		}else if(str.compare("stay_down")==0){
			return MOVE::STAY_DOWN;
		}else if(str.compare("stay_blocking")==0){
			return MOVE::STAY_BLOCKING;
		}else if(str.compare("stay_downblocking")==0){
			return MOVE::STAY_DOWN_BLOCKING;
		}
	}

	int keys_to_move(string str1,string str2){
	if(str1.compare("punch_press") == 0 && str2.compare("punch_press") == 0){
		return MOVE::DOUBLE_PUNCH;
	}else if(str1.compare("down_press")==0 && str2.compare("punch_press")==0){
		return MOVE::DOWN_PUNCH;
	}else if(str1.compare("down_press")==0 && str2.compare("kick_press")==0){
		return MOVE::DOWN_KICK;
	}else if(str1.compare("down_press")==0 &&str2.compare("block_press")==0){
		return MOVE::DOWN_BLOCK;
	}else if(str1.compare("up_press")==0 && str2.compare("kick_press")==0){
		return MOVE::HIGH_KICK;
	}else if(str1.compare("up_press")==0 &&str2.compare("punch_press")==0){
		return MOVE::HIGH_PUNCH;
	}else if(str1.compare("up_press")==0 &&str2.compare("block_press")==0){
		return MOVE::GRAB;
	}else if(str1.compare("up_press")==0 &&str2.compare("right_press")==0){
		if(player -> getDirection().compare("left") == 0){
			return MOVE::JumpRoll;
		}else{
			return MOVE::UP;
		}
	}else if(str1.compare("up_press")==0 &&str2.compare("left_press")==0){

		if(player -> getDirection().compare("left") == 0){
			return MOVE::UP;
		}else{
			return MOVE::JumpRoll;
		}
	}else if(str1.compare("right_press") == 0 && str2.compare("kick_press") == 0){
		cout << "patates2" << endl;
		if(player -> getDirection().compare("left") == 0){
			return MOVE::Sweep;
		}else{
			return MOVE::RIGHT;
		}
	}else if(str1.compare("left_press") == 0 && str2.compare("kick_press") == 0){
		cout << "patates" << endl;
		if(player -> getDirection().compare("left") == 0){
			return MOVE::LEFT;
		}else{
			return MOVE::Sweep;
		}
	}else{
		cout << "EROROROROROROOROR" << endl;
	}
}


	int getInput(vector<string> keyBuffer){
		count++;
		int move=-666;

		//cout<<"lalalala"<<endl;
	//	for(int j=0; j<keyBuffer.size(); j++){cout<<"pressed->"<<keyBuffer.at(j)<<endl;}
		if(keyBuffer.size()==0 && mvState==STATE::MOVING_RIGHT){count=0;return key_to_move("right_press");}else if(keyBuffer.size()==0 && mvState==STATE::MOVING_LEFT){count=0;return key_to_move("left_press");}else if(keyBuffer.size()==0 && mvState==STATE::DUCK){cout<<"Entered hereeeeee44444444"<<endl;count=0; return key_to_move("stay_down");}else if(keyBuffer.size()==0 && mvState==STATE::BLOCKING){cout<<"Entered hereeeeee8888888888"<<endl;count=0; return key_to_move("stay_blocking");}else if(keyBuffer.size()==0 && mvState==STATE::DOWNBLOCKING){cout<<"Entered hereeeeee8888888888"<<endl;count=0; return key_to_move("stay_downblocking");}
		if(count>3){
			if(localBuffer.size()==1){
				//cout<<"execute:"<<localBuffer.at(0)<<" restart the creation of time frames"<<endl;
				move=key_to_move(localBuffer.at(0));
			}
			localBuffer.clear();
			count=0;
			for(int j=0; j<keyBuffer.size(); j++){
						if(keyBuffer.at(j).compare("block_release")==0 && mvState==STATE::BLOCKING){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("left_release")==0 && mvState==STATE::MOVING_LEFT){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("right_release")==0 && mvState==STATE::MOVING_RIGHT){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("down_release")==0 && mvState==STATE::DUCK){mvState=STATE::NONE;}
			}
			return move;
		}
		for(int i=0; i<keyBuffer.size(); i++){
			if(mvState==STATE::NONE){
			if(localBuffer.size()==0){
				if(keyBuffer.at(i).compare("sp")==0){
					return SP_MOVE;
				}
				if(makes_combo(keyBuffer.at(i))==true){
					localBuffer.push_back(keyBuffer.at(i));
				}else{
					count=0;
					//cout<<"execute:"<<keyBuffer.at(i)<<" restart the creation of time frames"<<endl;
					if(keyBuffer.at(i).compare("right_press")==0){mvState=STATE::MOVING_RIGHT;}else if(keyBuffer.at(i).compare("left_press")==0){mvState=STATE::MOVING_LEFT;}else if(keyBuffer.at(i).compare("down_press")==0){mvState=STATE::DUCK;}else if(keyBuffer.at(i).compare("block_press")==0){mvState=STATE::BLOCKING;}
					/*check the buffer for releases OR MAYBE NOT I'LL SEE*/
					move=key_to_move(keyBuffer.at(i));
					localBuffer.clear();
					//here
					for(int j=i; j<keyBuffer.size(); j++){
						if(keyBuffer.at(j).compare("block_release")==0 /*&& mvState==STATE::BLOCKING*/){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("left_release")==0 && mvState==STATE::MOVING_LEFT){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("right_release")==0 && mvState==STATE::MOVING_RIGHT){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("down_release")==0 && mvState==STATE::DUCK){mvState=STATE::NONE;}
					}
					return move;
				}
			}else if(localBuffer.size()==1){
				if(promising_special_move(localBuffer.at(0),keyBuffer.at(i))==true){
					localBuffer.push_back(keyBuffer.at(i));
				}else if(make_combo_of_two(localBuffer.at(0),keyBuffer.at(i))==true){
					cout<<"execute combo:"<<localBuffer.at(0)<<","<<keyBuffer.at(i)<<" restart the creation of time frames"<<endl;
					count=0;
					move=keys_to_move(localBuffer.at(0),keyBuffer.at(i));
					localBuffer.clear();
					for(int j=i; j<keyBuffer.size(); j++){
						if(keyBuffer.at(j).compare("block_release")==0 && mvState==STATE::BLOCKING){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("left_release")==0 && mvState==STATE::MOVING_LEFT){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("right_release")==0 && mvState==STATE::MOVING_RIGHT){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("down_release")==0 && mvState==STATE::DUCK){mvState=STATE::NONE;}
					}
					return move;
				}else{
					//cout<<"execute:"<<localBuffer.at(0)<<" restart the creation of time frames"<<endl;
					count=0;
					move=key_to_move(localBuffer.at(0));
					localBuffer.clear();
					for(int j=i; j<keyBuffer.size(); j++){
						if(keyBuffer.at(j).compare("block_release")==0 && mvState==STATE::BLOCKING){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("left_release")==0 && mvState==STATE::MOVING_LEFT){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("right_release")==0 && mvState==STATE::MOVING_RIGHT){mvState=STATE::NONE;}
						else if(keyBuffer.at(j).compare("down_release")==0 && mvState==STATE::DUCK){mvState=STATE::NONE;}
					}
					return move;
				}
			}else{
				if(validate_special_move(localBuffer.at(0),localBuffer.at(1),keyBuffer.at(i))==true){
					//cout<<"execute special  move:"<<localBuffer.at(0)<<localBuffer.at(1)<<keyBuffer.at(i)<<" restart the creation of time frames"<<endl;
					localBuffer.clear();
					count=0;
					move=MOVE::SP_MOVE;
					return move;
				}else{
					//cout<<"execute:"<<localBuffer.at(0)<<" restart the creation of time frames"<<endl;
					count=0;
					move=key_to_move(localBuffer.at(0));
					localBuffer.clear();
					return move;
				}
			}

			}else if(mvState==STATE::DUCK){

				cout<<"in duck state";
			//	player->setPlayerHeight(60);
				if(keyBuffer.at(i).compare("down_release")==0){
					//cout<<"RELEASEDDDDDDD111111111111111111111111111111111111111111111";
					mvState=STATE::NONE;
				}else if(keyBuffer.at(i).compare("punch_press")==0 || keyBuffer.at(i).compare("kick_press")==0){
					for(int j=i; j<keyBuffer.size(); j++){
						if(keyBuffer.at(j).compare("down_release")==0 /*&& mvState==STATE::BLOCKING*/){cout<<"entered"<<endl;mvState=STATE::NONE;}
					}
					return keys_to_move("down_press",keyBuffer.at(i));
				}else if(keyBuffer.at(i).compare("block_press")==0){
						mvState=STATE::DOWNBLOCKING;
						for(int j=i; j<keyBuffer.size(); j++){
							if(keyBuffer.at(j).compare("block_release")==0 /*&& mvState==STATE::BLOCKING*/){cout<<"entered"<<endl;mvState=STATE::DUCK;}
						}
						return keys_to_move("down_press","block_press");
				}else{
					for(int j=i; j<keyBuffer.size(); j++){
						if(keyBuffer.at(j).compare("down_release")==0 /*&& mvState==STATE::BLOCKING*/){cout<<"entered"<<endl;mvState=STATE::NONE;}
					}
					return key_to_move("down_press");
					//return MOVE::STAY_DOWN;
				}


			}else if(mvState==STATE::BLOCKING){
				//cout<<"in block state";
				if(keyBuffer.at(i).compare("block_release")==0){
					//cout<<"RELEASEDDDDDDD111111111111111111111111111111111111111111111";
					mvState=STATE::NONE;
				}else{
					for(int j=i; j<keyBuffer.size(); j++){
						if(keyBuffer.at(j).compare("block_release")==0 /*&& mvState==STATE::BLOCKING*/){cout<<"entered"<<endl;mvState=STATE::NONE;}
					}
					return MOVE::STAY_BLOCKING;
				}

			}else if(mvState==STATE::DOWNBLOCKING){
				//cout<<"in down block state";
				if(keyBuffer.at(i).compare("block_release")==0){
					//cout<<"RELEASEDDDDDDD111111111111111111111111111111111111111111111";
					mvState=STATE::DUCK;
				}else{
					for(int j=i; j<keyBuffer.size(); j++){
						if(keyBuffer.at(j).compare("block_release")==0 /*&& mvState==STATE::BLOCKING*/){cout<<"entered"<<endl;mvState=STATE::DUCK;}
					}
					return MOVE::STAY_DOWN_BLOCKING;
				}

			}else{
				//cout<<"in moving state";
				if(mvState==STATE::MOVING_RIGHT){
					if(keyBuffer.at(i).compare("right_release")==0){
						//cout<<"RELEASEDDDDDDD";
						mvState=STATE::NONE;
					}else if(keyBuffer.at(i).compare("kick_press")==0 ){
						mvState=STATE::NONE;
						return keys_to_move("right_press","kick_press");
					}else if(keyBuffer.at(i).compare("punch_press")==0 || keyBuffer.at(i).compare("block_press")==0){
						//make a chek for keys that will stop the movement
						mvState=STATE::NONE;
						return key_to_move(keyBuffer.at(i)); //incase of movement we have to stop the animation and execute the new one or not (we'll decide later)
					}else{
						for(int j=i; j<keyBuffer.size(); j++){
							if(keyBuffer.at(j).compare("right_release")==0 /*&& mvState==STATE::BLOCKING*/){cout<<"entered"<<endl;mvState=STATE::NONE;}
						}
						return key_to_move("right_press");
					}
				}
				if(mvState==STATE::MOVING_LEFT){
					if(keyBuffer.at(i).compare("left_release")==0){
						//cout<<"RELEASEDDDDDDD";
						mvState=STATE::NONE;
					}else if(keyBuffer.at(i).compare("kick_press")==0 ){
						mvState=STATE::NONE;
						return keys_to_move("left_press","kick_press");
					}else if(keyBuffer.at(i).compare("punch_press")==0 ||
						keyBuffer.at(i).compare("block_press")==0){
						//make a chek for keys that will stop the movement
						mvState=STATE::NONE;
						return key_to_move(keyBuffer.at(i)); //incase of movement we have to stop the animation and execute the newone (we'll decide later)
					}else{
						for(int j=i; j<keyBuffer.size(); j++){
							if(keyBuffer.at(j).compare("left_release")==0 /*&& mvState==STATE::BLOCKING*/){cout<<"entered"<<endl;mvState=STATE::NONE;}
						}
						return key_to_move("left_press");
					}
				}


			}
		}

		return 666;




};



	bool executeInput(Animator* anime,SpMoveAnimator *sp_anime,int instruction,int opponentInstruction,boolean collision){
		player->setDamage(0);
		bool executed=false;
		bool opponentIsDown=false;
		if(player->isStunned()){
		//	anime->startAction("VictoryFinal");
			return true;
		}
		//DOWN  DOWN_PUNCH DOWN_BLOCK DOWN_KICK STAY_DOWN STAY_DOWN_BLOCKING
		if(opponentInstruction==MOVE::DOWN||opponentInstruction==MOVE::DOWN_PUNCH||opponentInstruction==MOVE::DOWN_BLOCK||opponentInstruction==MOVE::DOWN_KICK||opponentInstruction==MOVE::STAY_DOWN||opponentInstruction==MOVE::STAY_DOWN_BLOCKING){
			opponentIsDown=true;
		}
		//player->setBlocking(false);
		//bool executed=false;
		if(player->getHealth() >0){
			if(!player->isWinner()){
				switch(instruction){
					case MOVE::UP:
						if(player->getDirection()=="left"){
							anime -> startAction("LeftJump");
						}else{
							anime->startAction("RightJump");
						}
						break;
					case MOVE::DOWN:
						if(player->getDirection()=="left")
							anime->startAction("LeftDuck");
						else
							anime->startAction("RightDuck");
						break;
					case MOVE::LEFT:
						if(player->getDirection()=="left")
							anime->startAction("LeftBackwards");
						else
							anime->startAction("RightForward");
						break;
					case MOVE::RIGHT:
						if(player->getDirection()=="left")
							anime->startAction("LeftForward");
						else
							anime->startAction("RightBackwards");
						break;
					case MOVE::PUNCH:
						//sound_manager.playSound(2);
						//sound_manager.setPlaySound(2,true);
					//	sound_manager.playSound(2);
						if(player->getDirection()=="left"){
							executed=anime->startAction("LeftMiddleRightPunch");
							if(executed){
								if(!collision)
									player->updateHitbox(50,0,0,0);
								if(opponentIsDown==false){
									player->setDamage(10);
								}
							}
						}
						else{
							executed=anime->startAction("RightMiddleRightPunch");
							if(executed){
								if(!collision)
									player->updateHitbox(-50,0,0,0);
								if(opponentIsDown==false){
									player->setDamage(10);
								}
							}
						}
						break;
					case MOVE::HIGH_PUNCH:
						//sound_manager.playSound(2);
						if(player->getDirection()=="left"){

							executed=anime->startAction("LeftHighLeftPunch");
							if(executed){
								if(!collision)
									player->updateHitbox(50,0,0,0);
								if(opponentIsDown==false){
									player->setDamage(20);
								}
							}
						}
						else{
							executed=anime->startAction("RightHighLeftPunch");
							if(executed){
								if(!collision)
									player->updateHitbox(-50,0,0,0);
								if(opponentIsDown==false){
									player->setDamage(20);
								}
							}
						}
						break;
					case MOVE::DOUBLE_PUNCH:
						//sound_manager.playSound(2);
						if(player->getDirection()=="left"){
							executed=anime -> startAction("LeftMiddleDoublePunch");
							if(executed){
								if(!collision)
									player->updateHitbox(50,0,0,0);
								if(opponentIsDown==false){
									player->setDamage(20);
								}
							}
						}else{
							executed=anime -> startAction("RightMiddleDoublePunch");
							if(executed){
								if(!collision)
									player->updateHitbox(-50,0,0,0);
								if(opponentIsDown==false){
									player->setDamage(20);
								}
							}
						}
						break;
					case MOVE::KICK:
					//	sound_manager.playSound(2);
						if(player->getDirection()=="left"){
							executed=anime->startAction("LeftMiddleLeftBackKick");
							if(executed){
								if(!collision)
									player->updateHitbox(60,0,0,0);
								if(opponentIsDown==false){
									player->setDamage(10);
								}
							}
						}
						else{
							executed=anime->startAction("RightMiddleLeftBackKick");
							if(executed){
								if(!collision)
									player->updateHitbox(-60,0,0,0);
								if(opponentIsDown==false){
									player->setDamage(10);
								}
							}
						}
						break;
					case MOVE::BLOCK:
							player->setBlocking(true);
						if(player->getDirection()=="left"){

							anime->startAction("LeftBlock");
						}
						else{
							anime->startAction("RightBlock");
						}
						break;
					case MOVE::DOWN_PUNCH:
						//sound_manager.playSound(2);
						if(player->getDirection()=="left"){
							executed=anime->startAction("LeftUppercut");
							if(executed){
								if(!collision)
									player->updateHitbox(55,0,0,0);
								player->setDamage(10);

							}
						}else{
							executed=anime->startAction("RightUppercut");
							if(executed){
								if(!collision)
									player->updateHitbox(-55,0,0,0);
								player->setDamage(10);

							}
						}
						break;
					case MOVE::DOWN_BLOCK:
						player->setBlocking(true);
						if(player->getDirection()=="left"){
							anime->startAction("LeftDuckBlock");
						}else{
							anime->startAction("RightDuckBlock");
						}
						break;
					case MOVE::DOWN_KICK:
						//sound_manager.playSound(2);
						if(player->getDirection()=="left"){
							executed=anime->startAction("LeftLowKick");
							if(executed){
								if(!collision)
									player->updateHitbox(55,0,0,0);
								player->setDamage(10);
							}
						}else{
							executed=anime->startAction("RightLowKick");
							if(executed){
								if(!collision)
									player->updateHitbox(-55,0,0,0);
								player->setDamage(10);
							}
						}
						break;
					case MOVE::SP_MOVE:
					cout<<"SPECIAL COMBO!!"<<endl;
					//specialMove

						//special_move->printHitbox();
					//	player->getSpecial()->printHitbox();
						if(player->getName().compare("p1")==0){
							if(player->getDirection()=="left"){
								anime->startAction("LeftFreezeBallMove");
								sp_anime->startAction();

							}else{
								anime->startAction("RightFreezeBallMove");
								sp_anime->startAction();
							}
					}
					else{
						if(sound_manager.canPlaySound(7))
							sound_manager.playSound(7);
						if(player->getDirection()=="left"){
							anime->startAction("LeftSpecial");
							sp_anime->startAction();

						}else{
							anime->startAction("RightSpecial");
							sp_anime->startAction();
						}
					}
						break;
						case MOVE::HIGH_KICK:
							sound_manager.playSound(2);
							if(player->getDirection()=="left"){
								executed=anime->startAction("LeftHighBackKick");
								if(executed){
									if(!collision)
										player->updateHitbox(55,0,0,0);
									if(opponentIsDown==false){
										player->setDamage(20);
									}
								}
							}else{
								executed=anime->startAction("RightHighBackKick");
								if(executed){
									if(!collision)
										player->updateHitbox(-55,0,0,0);
									if(opponentIsDown==false){
										player->setDamage(20);
									}
								}
							}
						break;
						case MOVE::JumpRoll:
							if(player->getDirection()=="left")
								anime->startAction("LeftJumpRollMidAir");
							else
								anime->startAction("RightJumpRollMidAir");
						break;
						case MOVE::GRAB:
							if(player->getDirection()=="left")  //den vgazei to grab
								anime->startAction("LeftGrab");
							else
								anime->startAction("RightGrab");
						break;
						case MOVE::STAY_DOWN:
							if(player->getDirection()=="left")
								anime->startAction("StayLeftDown");
							else
								anime->startAction("StayRightDown");
						break;
						case MOVE::STAY_BLOCKING:
							player->setBlocking(true);
							if(player->getDirection()=="left")
								anime->startAction("LeftStayBlocking");
							else
								anime->startAction("RightStayBlocking");
						break;
						case MOVE::STAY_DOWN_BLOCKING:
							player->setBlocking(true);
							if(player->getDirection()=="left")
								anime->startAction("StayLeftDuckBlock");
							else
								anime->startAction("StayLRightDuckBlock");
						break;
						case MOVE::Sweep:
							if(player -> getDirection() == "left"){
								executed=anime -> startAction("LeftSweep");
								if(executed){
									if(!collision)
										player->updateHitbox(50,0,0,0);
									player->setDamage(10);
								}
							}else{
								executed=anime -> startAction("RightSweep");
								if(executed){
									if(!collision)
										player->updateHitbox(-50,0,0,0);
									player->setDamage(10);
								}
							}
						break;
					}
				//	cout<<"executed::::::::::"<<executed<<endl;
				}
				else{
					cout<<"Winner"<<endl;
					anime->startAction("Victory");
				}
			}
			else{
				if(player->hasLives())
					anime->startAction("RightFrozenDead");
					//player->DecrementLives();
				else
					anime->startAction("RightFrozenDeadFinal");
					//anime->startAction("fatality");
			}
			if(instruction == 17 || instruction == 6 || instruction == 9 || instruction == 18 )
				player->setBlocking(true);
			else
				player->setBlocking(false);

			return executed;

		}




	void executeGotHit(Animator* anime,int attacker_instruction,int this_instruction){


			if(this_instruction!=MOVE::BLOCK || this_instruction!=MOVE::DOWN || this_instruction!=MOVE::DOWN_BLOCK ){
			//if(player->getHealth()<=0){cout<<"popaaaa"<<endl;anime->startAction("LeftGettingHitPunch"); return;} //works but need some fixes
			if(attacker_instruction==MOVE::PUNCH  ){
				sound_manager.playSound(3);
				//sound_manager->playSoundGotHit();
				if(player->isStunned())
					player->setStunned(false);
					if(player->getDirection()=="left"){

						anime->startAction("LeftGettingHitPunch");
					}else{
						anime->startAction("RightGettingHitPunch");
					}
			}else if(attacker_instruction==MOVE::DOUBLE_PUNCH  ){
				sound_manager.playSound(3);
					//sound_manager->playSoundGotHit();
					if(player->isStunned())
						player->setStunned(false);
					if(player->getDirection()=="left"){
						anime->startAction("LeftGettingHitDPunch");
					}else{
						anime->startAction("RightGettingHitDPunch");
					}

				}else if(attacker_instruction==MOVE::HIGH_KICK ){
					sound_manager.playSound(3);
					if(player->isStunned())
						player->setStunned(false);
					if(player->getDirection()=="left"){
						anime->startAction("LeftGettingHitHighKick");
					}else{
						anime->startAction("RightGettingHitHighKick");
					}

				}else if(attacker_instruction==MOVE::HIGH_PUNCH){
					sound_manager.playSound(3);
					if(player->isStunned())
						player->setStunned(false);
					if(player->getDirection()=="left"){
						anime->startAction("LeftGettingHitHighPunch");
					}else{
						anime->startAction("RightGettingHitHighPunch");
					}
				}else if(attacker_instruction==MOVE::KICK){
					sound_manager.playSound(3);
					if(player->isStunned())
						player->setStunned(false);
					if(player->getDirection()=="left"){
						anime->startAction("LeftGettingHitKick");
					}else{
						anime->startAction("RightGettingHitKick");
					}
				}else if(attacker_instruction==MOVE::DOWN_PUNCH ){
					sound_manager.playSound(3);
					if(player->isStunned())
						player->setStunned(false);
					if(player->getDirection()=="left"){
					//	cout<<"hrdgrbfgdrhberdkdsjhfgusrhfkjsehfksehfiusehufyhwrfjweoyfhiweofjgwefhwekjfgwuehfgyweg"<<endl;
						//anime->startAction("LeftGettingHitUppercut");
						anime->startAction("LeftGettingHitUppercut");
					}else{
						anime->startAction("RightGettingHitUppercut");
					//	cout<<"hrdgrbfgdrhberdkdsjhfgusrhfkjsehfksehfiusehufyhwrfjweoyfhiweofjgwefhwekjfgwuehfgyweg"<<endl;
						//anime->startAction("RightGettingUpDownKnockout");

					}
				}else if(attacker_instruction==MOVE::Sweep){
					sound_manager.playSound(3);
					if(player->isStunned())
						player->setStunned(false);
					if(player->getDirection()=="left"){
						anime->startAction("LeftGettingUpDownKnockout");
					}else{
						anime->startAction("RightGettingUpDownKnockout");
					}
				}else if(attacker_instruction==MOVE::DOWN_KICK){
					sound_manager.playSound(3);
					if(player->isStunned())
						player->setStunned(false);
					if(player->getDirection()=="left"){
						anime->startAction("LeftGettingHitLow");
					}else{
						anime->startAction("RightGettingHitLow");
					}
				}else if(attacker_instruction==MOVE::SP_MOVE){
					sound_manager.playSound(6);
					if(player->getDirection()=="left"){
						//anime->startAction("LeftGettingHitPunch");
					}else{
						//anime->startAction("RightGettingHitPunch");
					}

				}//else{
					//printf("Should not entered here::UNPREDICTED MOVE-CHECK executeGotHit() CASES AGAIN");
				//}
			}else{
				//what hit animation should we use when the player gets hit whil ducking?

			}
	}


	void playSound(int move){

			if(move == 0 || move == 5 || move == 7 || move == 8 || move == 10 || move == 11 || move == 12 || move == 13 || move==19){

				if(!player->isStunned())
					sound_manager.playSound(2);
			}

	}





};
