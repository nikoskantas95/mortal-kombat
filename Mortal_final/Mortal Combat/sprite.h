#ifndef __SPRITE_H__
#define __SPRITE_H__

#include <iostream>

#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>
#include <allegro5\allegro_audio.h>
#include <allegro5\allegro_acodec.h>
#include <string.h>
using namespace std;



class sprite{
/*private:
	unsigned x;
	unsigned y;
	unsigned width;
	unsigned height;
	unsigned positionX;
	unsigned positionY;
	ALLEGRO_BITMAP* imageBM;*/

public:
	unsigned x;
	unsigned y;
	unsigned width;
	unsigned height;
	unsigned positionX;
	unsigned positionY;
	ALLEGRO_BITMAP* imageBM;
	sprite(ALLEGRO_BITMAP* bm, unsigned sx, unsigned sy, unsigned ex, unsigned ey, unsigned px, unsigned py){
		x = sx;
		y = sy;
		width = ex;
		height = ey;
		positionX = px;
		positionY = py;
		imageBM = bm;
	}

	void newSprite(string player){
		if(player.compare("p1")==0){
			//cout<<"new sprite 1";
			x = 20;
			y = 20;
			width = 42;
			height = 100;
			positionX = 100;
			positionY = 100;
		}
		else{
			//cout<<"new sprite2"<<endl;
			x = 20;
			y = 20;
			width = 42;
			height = 100;
			positionX = 600;
			positionY = 100;
		}

	}

	ALLEGRO_BITMAP *getBM(){
		return imageBM;
	}



	void draw(){
		al_draw_bitmap_region(imageBM, x, y, width, height, positionX, positionY, 0);
	}

	void move(unsigned x,unsigned width,unsigned y,unsigned height){

	}


	void toString(){
		std::cout << "Image"<<std::endl;
		std::cout << "Position (" << positionX << ", " << positionY <<")"<< std::endl;
	}

};

#endif
